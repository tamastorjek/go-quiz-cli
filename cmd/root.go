package cmd

import (
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "go-quiz",
	Short: "Go Quiz",
	Long:  "A simple quiz application.",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize()

	rootCmd.PersistentFlags().StringP(
		"answerLabel",
		"l",
		"numbers",
		"Select answers by <numbers> (1,2,3) or <letters> (a,b,c) (default: numbers).",
	)
}
