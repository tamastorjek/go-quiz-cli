package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/tamastorjek/go-quiz/src/quiz"
	"gitlab.com/tamastorjek/go-quiz/src/services"

	// todo: maybe this should be in its own folder
	baseApi "gitlab.com/tamastorjek/go-quiz/src/api"
	// this can be switched with another implementation, as long as it correctly implements IQuizApi,
	// and has an exported `New` method
	apiProvider "gitlab.com/tamastorjek/go-quiz/src/api/opentdb"
)

var flags *services.AppFlags
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Start a quiz.",
	Long:  "Have fun!",
	Args: func(cmd *cobra.Command, args []string) error {
		answerLabel := cmd.Flag("answerLabel").Value.String()

		if !quiz.IsValidAnswerLabel(answerLabel) {
			return fmt.Errorf("invalid answer label specified: %s", answerLabel)
		}

		flags = services.GetInstance()
		flags.Data["answerLabel"] = answerLabel

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		// set up API, passing selected provider
		// then directly get the quiz
		quiz := baseApi.New(apiProvider.New()).GetQuiz()
		reader := bufio.NewReader(os.Stdin)

		for quiz.HasMoreQuestions() {
			var answer string
			question := quiz.NextQuestion()
			showQuestion(question)

			answer = getAnswer(reader)

			for !quiz.SelectAnswer(answer) {
				fmt.Printf("Invalid answer (%s)", answer)
				answer = getAnswer(reader)
			}

			fmt.Println("--------------------")
			fmt.Println("")
		}

		total, correct := quiz.CheckCorrectAnswers()

		fmt.Printf("You answered %d/%d questions correctly (%.2f)", correct, total, float64(correct)/float64(total)*100)
	},
}

func init() {
	rootCmd.AddCommand(startCmd)
}

func showQuestion(question *quiz.Question) {
	fmt.Println(question.Text)
	fmt.Println("")

	label := quiz.GetIntLabel()

	for _, answer := range question.Answers {
		fmt.Printf("%s) %s\n", string(label), answer)
		label++
	}
}

func getAnswer(reader *bufio.Reader) string {
	fmt.Println("")
	fmt.Print("My answer is: ")

	answer, _ := reader.ReadString('\n')

	return strings.TrimSpace(answer)
}
