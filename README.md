# Small GO Quiz CLI App

## Description
A small CLI app for a quixk quiz.

## Usage
- Clone repository
- `go install`
- `go-quiz start <args>` -- _Valid arguments:_
  - `--answerLabel` or `l`, Select answers by _**numbers**_ (1,2,3) or _**letters**_ (a,b,c) (default: numbers). Example: `go-quiz start -l letters`