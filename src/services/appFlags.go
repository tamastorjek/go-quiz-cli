package services

import (
	"sync"
)

type AppFlags struct {
	Data map[string]string
}

var instance *AppFlags
var once sync.Once

func GetInstance() *AppFlags {
	once.Do(func() {
		instance = &AppFlags{make(map[string]string)}
	})
	return instance
}

// a bit easier on the eyes to get a flag
// here we assume that all the flags are strings for now
func Get(key string) string {
	value, exists := instance.Data[key]

	if exists {
		return value
	}

	panic("Error: invalid key")
}
