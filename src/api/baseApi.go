package baseApi

import (
	"sync"

	"gitlab.com/tamastorjek/go-quiz/src/quiz"
)

type IBaseApi interface {
	GetQuiz() quiz.Quiz
}

var instance IBaseApi
var once sync.Once

func New(provider IBaseApi) IBaseApi {
	once.Do(func() {
		instance = provider
	})

	return instance
}

func GetInstance() *IBaseApi {
	return &instance
}
