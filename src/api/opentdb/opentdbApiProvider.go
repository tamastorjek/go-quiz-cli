package opentdb

import (
	"encoding/json"
	"fmt"
	"html"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/tamastorjek/go-quiz/src/quiz"
	"gitlab.com/tamastorjek/go-quiz/src/services"
)

var config = map[string]string{
	"apiUrl": "https://opentdb.com/api.php?amount=3&category=11&difficulty=easy&type=multiple",
}

type OpenTDBQuizApiProvider struct {
	apiUrl string
}

func New() *OpenTDBQuizApiProvider {
	return &OpenTDBQuizApiProvider{apiUrl: config["apiUrl"]}
}

func (api *OpenTDBQuizApiProvider) GetQuiz() quiz.Quiz {
	responseData := makeRequest(api.apiUrl, http.MethodGet)
	entity := QuizEntity{}

	if error := json.Unmarshal(responseData, &entity); error != nil {
		fmt.Printf("Error while decoding response: %v", error)
	}

	return convertEntity(entity)
}

func makeRequest(url string, method string) []byte {
	request, error := http.NewRequest(method, url, nil)

	if error != nil {
		fmt.Printf("Error while creating request: %v", error)
	}

	request.Header.Add("Accept", "application/json")

	response, error := http.DefaultClient.Do(request)

	if error != nil {
		fmt.Printf("Error while executing request: %v", error)
	}

	responseData, error := ioutil.ReadAll(response.Body)

	if error != nil {
		fmt.Printf("Error while reading response body: %v", error)
	}

	return responseData
}

func convertEntity(entity QuizEntity) quiz.Quiz {
	return quiz.NewQuiz(getQuestionsFromEntity(entity), services.Get("answerLabel"))
}

func getQuestionsFromEntity(entity QuizEntity) []quiz.Question {
	var questions []quiz.Question

	for _, entityQuestions := range entity.Results {
		// combine answers and randomize them
		answers := shuffleAnswers(
			append(entityQuestions.IncorrectAnswers, entityQuestions.CorrectAnswer),
		)
		questions = append(
			questions,
			quiz.NewQuestion(
				html.UnescapeString(entityQuestions.Question),
				answers,
			),
		)
	}

	return questions
}

func shuffleAnswers(answers []string) []string {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(answers), func(i, j int) { answers[i], answers[j] = answers[j], answers[i] })

	for i, a := range answers {
		answers[i] = html.UnescapeString(a)
	}

	return answers
}
