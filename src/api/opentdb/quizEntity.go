package opentdb

type QuizEntity struct {
	ResponseCode int              `json:"response_code"`
	Results      []QuestionEntity `json:"results"`
}
