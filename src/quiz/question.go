package quiz

type IQuestion interface {
	SelectAnswer(string) bool
}

type Question struct {
	Text           string
	Answers        []string
	selectedAnswer int32
}

func NewQuestion(text string, answers []string) Question {
	return Question{Text: text, Answers: answers}
}

func (question *Question) SelectAnswer(answer string, answerLabel string) bool {
	answerIndex := getNumericAnswerIndex(answer, answerLabel)

	if !question.isValidAnswer(answer, answerLabel) {
		return false
	}

	question.selectedAnswer = answerIndex

	return true
}

func (question *Question) isValidAnswer(answer string, answerLabel string) bool {
	answerIndex := []rune(answer)[0]
	minAnswerIndex := getAnswerLabelBasePoint(answerLabel)
	maxAnswerIndex := minAnswerIndex + int32(len(question.Answers)) - 1

	if answerIndex >= minAnswerIndex && answerIndex <= maxAnswerIndex {
		return true
	}

	return false
}

func getAnswerLabelBasePoint(answerLabel string) int32 {
	// in case we want to add more label options...
	switch answerLabel {
	case "letters":
		return rune('a')
	}

	// default numbers
	return rune('1')
}

func getNumericAnswerIndex(answer string, answerLabel string) int32 {
	// strings can be only converted to slices of runes, so we need to get the first item
	return []rune(answer)[0] - getAnswerLabelBasePoint(answerLabel)
}
