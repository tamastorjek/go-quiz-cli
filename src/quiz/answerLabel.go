package quiz

import "gitlab.com/tamastorjek/go-quiz/src/services"

var validAnswerLabels [2]string = [2]string{"numbers", "letters"}

func IsValidAnswerLabel(answerLabel string) bool {
	for _, validAnswerLabel := range validAnswerLabels {
		if answerLabel == validAnswerLabel {
			return true
		}
	}

	return false
}

func GetIntLabel() int32 {
	if services.Get("answerLabel") == validAnswerLabels[0] {
		return '1'
	}

	return 'a'
}
