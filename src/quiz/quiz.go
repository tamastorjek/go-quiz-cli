package quiz

import (
	"math/rand"
	"time"
)

type IQuiz interface {
	HasMoreQuestions() bool
	NextQuestion() Question
	CheckCorrectAnswers() (int, int)
	SelectAnswer(string) bool
}

type Quiz struct {
	questions            []Question
	currentQuestionIndex int
	AnswerLabel          string
}

func NewQuiz(questions []Question, answerLabel string) Quiz {
	if !IsValidAnswerLabel(answerLabel) {
		// for now, simply use a default value
		answerLabel = validAnswerLabels[0]
	}

	return Quiz{questions: questions, AnswerLabel: answerLabel, currentQuestionIndex: -1}
}

func (quiz *Quiz) HasMoreQuestions() bool {
	return quiz.currentQuestionIndex < len(quiz.questions)-1
}

func (quiz *Quiz) NextQuestion() *Question {
	quiz.currentQuestionIndex++
	question := &quiz.questions[quiz.currentQuestionIndex]
	return question
}

func (quiz *Quiz) CheckCorrectAnswers() (total int, correct int) {
	questionCount := len(quiz.questions)

	rand.Seed(time.Now().UnixNano())

	return questionCount, rand.Intn(questionCount-1) + 1
}

func (quiz *Quiz) SelectAnswer(answer string) bool {
	if quiz.currentQuestionIndex < -1 || quiz.currentQuestionIndex > len(quiz.questions) {
		return false
	}

	return quiz.questions[quiz.currentQuestionIndex].SelectAnswer(answer, quiz.AnswerLabel)
}
